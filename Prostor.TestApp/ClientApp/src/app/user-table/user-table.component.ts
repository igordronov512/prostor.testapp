import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UserEditDialogComponent } from '../user-edit-dialog/user-edit-dialog.component';
import { UserRestClient } from './UserRestClient';

@Component({
    selector: 'app-user-table',
    templateUrl: './user-table.component.html',
    styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

    data: User[] = [];
    displayedColumns = ['firstName', 'lastName', 'actions'];

    addForm: FormGroup;



    constructor(private client: UserRestClient, private dialog: MatDialog, f: FormBuilder) {

        this.addForm = f.group({
            firstName: [null, Validators.required],
            lastName: [null, Validators.required]
        });
    }

    async ngOnInit() {
        this.refresh();
    }

    async add() {
        if (this.addForm.invalid) {
            alert("Форма заполнена некорректно");
            return;
        }

        try {
            await this.client.addUser(this.addForm.value).toPromise();
            this.addForm.reset();
        }
        catch (e: any) { alert(e.message); }

        this.refresh();
    }

    async refresh() {
        this.data = await this.client.getUsers().toPromise() ?? [];
    }

    async delete(row: User) {
        if (confirm(`Удалить пользователя ${row.firstName} ${row.lastName}`)) {
            try {
                await this.client.deleteUser(row.id).toPromise();
                this.refresh();
            }
            catch (e: any) { alert(e.message) }
        }
    }

    async edit(row: User) {
        let ref = this.dialog.open(UserEditDialogComponent, {
            data: row
        });

        let result = await ref.afterClosed().toPromise();

        if (result) {
            result.id = row.id;
            try {
                await this.client.updateUser(result).toPromise();
                this.refresh();
            }
            catch (e: any) { alert(e.message) } 
        }
    }
}

export interface User {
    id: number;
    firstName: string;
    lastName: string;
}
