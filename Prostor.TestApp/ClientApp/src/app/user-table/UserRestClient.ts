import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user-table.component';


@Injectable({ providedIn: 'root' })
export class UserRestClient {
    constructor(private http: HttpClient) { }


    public getUsers() {
        return this.http.get<User[]>('api/users');
    }

    public addUser(form: UserAddForm) {
        return this.http.post<User>('api/users', form);
    }


    deleteUser(id: number) {
        return this.http.delete(`api/users/${id}`);
    }

    updateUser(user: User) {
        return this.http.put(`api/users/${user.id}`, user);
    }
}

interface UserAddForm {
    firstName: string;
    lastName: string;
}
