import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../user-table/user-table.component';

@Component({
    selector: 'app-user-edit-dialog',
    templateUrl: './user-edit-dialog.component.html',
    styleUrls: ['./user-edit-dialog.component.css']
})
export class UserEditDialogComponent implements OnInit {
    form: FormGroup;

    constructor(f: FormBuilder, @Inject(MAT_DIALOG_DATA) data: User) {
        this.form = f.group({
            firstName: [null, Validators.required],
            lastName: [null, Validators.required]
        });

        this.form.patchValue(data);
    }

    ngOnInit(): void {
    }
}
